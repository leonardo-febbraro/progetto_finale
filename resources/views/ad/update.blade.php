<x-layout>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 d-flex justify-content-center">
                <h1 class="my-2">Modifica il tuo annuncio</h1>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-md-6">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="row justify-content-center mt-5 formCustom shadow">
                    <div class="col-12 col-md-4 d-flex justify-content-center formCustom1">
                        <form method="POST" action="{{ route('ad.edit', compact('ad')) }}"
                            enctype="multipart/form-data">
                            @csrf
                            @method("put")
                            <div class="mb-3 my-3">
                                <label class="form-check-label fw-bolder" for="exampleCheck1">Titolo</label>
                                <input name='title' type="text" class="form-control" value="{{ $ad->title }}">
                            </div>
                            {{-- PRezzo --}}
                            <div class="mb-3">
                                <label class="form-check-label fw-bolder" for="exampleCheck1">Prezzo</label>
                                <input name='price' type="number" class="form-control" value="{{ $ad->price }}">
                            </div>

                            <div class="mb-3">
                                <label for="exampleInputPassword1" class="form-label fw-bolder">Descrizione</label><br>
                                <textarea name="description" placeholder="scrivi qui la tua recensione..." id=""
                                    cols="53" rows="10">
                            {{ $ad->description }}
                                </textarea>
                            </div>


                            {{-- <div class="mb-3">
                                <select name="category">
                                    <option class="fw-bolder" value="" disabled selected>Seleziona una categoria</option>
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}">
                                            {{ $category->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div> --}}

                            {{-- <div class="mb-3">
                                <input type="file" name="img">
                            </div> --}}

                            <button type="submit" class="btn btn-primary mb-3">invia</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-layout>
