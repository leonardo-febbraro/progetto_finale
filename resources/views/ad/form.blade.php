<x-layout>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 d-flex justify-content-center">
                <h1 class="my-4">Inserisci il tuo annuncio</h1>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-6 d-flex align-items-center">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <section>
                <div class="d-flex flex-wrap justify-content-center">
                    <img class="img-fluid formImg" src="/img/ecommerce.png" alt="">
                    <h4 class="d-flex justify-content-center textAnnunc">Posta subito il tuo annuncio</h4>
                    <p class="d-flex justify-content-center textAnnunc2">Comincia a postare i tuoi annunci in modo semplice e veloce.
                    Troverai diversi tipi di articoli di vari prodotti.
                    </p>
                </div>
                    
                    <ul class="listSquare">
                        <li>
                            <p class="text-center">
                                Scatta una bella fotografia.
                            </p>
                        </li>
                        <li>
                            <p class="text-center">
                                Inserisci un prezzo realistico.
                            </p>
                        </li>
                        <li>
                            <p class="text-center">
                               Scrivi un annuncio chiaro.
                            </p>
                        </li>
                        <li>
                            <p class="text-center">
                                Incontra di persona e concludi il tuo affare.
                            </p>
                        </li>
                    </ul>
            </section>
            </div>

                <div class="col-12 col-md-6 formCustom">
                    <div class="formCustom1">
                        <form method="POST" action="{{ route('new.ad') }}" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden"
                            name="uniqueSecret"
                            value="{{$uniqueSecret}}">
                            <div class="mt-2">
                                <label class="form-check-label fw-bolder" for="exampleCheck1">Titolo</label>
                                <input name='title' type="text" class="form-control radiusForm" value="{{ old('title') }}">
                            </div>
                            {{-- PRezzo --}}
                            <div class="mt-2">
                                <label class="form-check-label fw-bolder" for="exampleCheck1">Prezzo</label>
                                <input name='price' type="number" class="form-control radiusForm" value="{{ old('price') }}">
                            </div>

                            <div class="mt-2">
                                <label for="exampleInputPassword1" class="form-label fw-bolder">Descrizione</label><br>
                                <textarea class="radiusForm" name="description" placeholder="scrivi qui la tua recensione..." id=""
                                    cols="53" rows="10">
                            {{ old('description') }}
                                </textarea>
                            </div>


                            <div class="mt-2">
                                <select class="categoryCustom" name="category">
                                    <option class="fw-bolder" value="" disabled selected>Seleziona una categoria
                                    </option>
                                    @foreach ($categories as $category)
                                        <option class="categoryCustom2" value="{{ $category->id }}">
                                            {{ $category->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div id="drophere" class="mt-4 dropzone"></div>
                            <div class="form-group row">
                                <label for="images" class="col-md-12 col-form-label text-md-right"></label>
                                <div class="col-md-12">
                                  
                                    @error("images")
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{$message}}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <button type="submit" class="btn btnDetail mb-3">invia</button>

                        </form>
                    </div>
                </div>
           
        </div>
    </div>
</x-layout>
