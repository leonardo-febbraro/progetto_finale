<x-layout>
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex justify-content-center my-4">
                <h1>Cerca l'annuncio per te</h1>
            </div>
        </div>
        @if (session('message'))
            <div class="alert alert-success">
                {{ session('message') }}
            </div>
        @endif
        @if (session('messageDelete'))
            <div class="alert alert-danger">
                {{ session('messageDelete') }}
            </div>
        @endif
        <div class="row">
            @foreach ($ads as $ad)
                <div class="col-12 col-md-4 my-3 d-flex justify-content-center rounded">
                    <div class="card shadow" style="width: 18rem;">
                        <div class="card-header cardColor">
                            <h5 class="titleCard card-title text-truncate justify-content-start ">{{ $ad->title }}
                            </h5>

                        </div>
                        <div class="card-body cardColor">
                            <h5 class="priceCard card-text">{{ $ad->price }}€</h5>
                            <h6 class="card-text text-truncate">Venditore: {{ $ad->user->name }}</h6>
                            <div class="card-img d-flex justify-content-center">
                                
                                <img class='img-fluid w-40' src="{{($ad->images->first()->getUrl(300,150))}}" alt="">

                            </div>
                            {{-- <p class="card-text">{{$ad->created_at->format("d/m/y")}}</p> --}}
                            <p class="card-text my-3 text-truncate">{{ $ad->description }}</p>
                            <div class="d-flex justify-content-end">
                                <a href="{{ route('ad.show', compact('ad')) }}" class="btn btnDetail">Dettaglio</a>
                            </div>
                        </div>
                        <div class="card-footer border-top-dark cardColor">
                            <span>Categoria:</span>
                            <a href="{{ route('ad.categories', [$ad->category->name, $ad->category->id]) }}"
                                class="btn ">{{ $ad->category->name }}</a>
                        </div>
                    </div>

                </div>
            @endforeach
        </div class='d-flex '>
        {{ $ads->links() }}
    </div>

</x-layout>
