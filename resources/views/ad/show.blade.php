<x-layout>
    {{-- @foreach ($ad->images as $element)
    @dd($element->file)
        
    @endforeach --}}
    <!-- Page Heading -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 d-flex justify-content-center">
                <h1 class="my-4">Dettaglio articoli</h1>
            </div>
        </div>
    </div>

    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif

    <div class="container-fluid">

        <div class="row">



            {{-- <img class="img-details" src="" alt=""> --}}
            <div class="col-12 col-md-6 my-5 d-flex align-items-end flex-column text-end">
                <div class="card-body ">
                    <h5 class="card-title">
                        {{ $ad->title }}
                    </h5>
                    <h4 class="card-title">
                        {{ $ad->price }}€
                    </h4>
                    <p class="card-text">{{ $ad->description }}</p>
                    <a href="{{ route('ad.categories', [$ad->category->name, $ad->category->id]) }}"
                        class="btn ">{{ $ad->category->name }}</a>
                    <p>{{ $ad->created_at->format('d/m/Y') }}</p>
                    <p class="card-text">{{ $ad->user->name }}</p>
                    <div>
                        <a href="{{ route('ad.allAds') }}" class="btn btnDetail">Torna indietro</a>
                        @auth
                            @if (Auth::user()->name == $ad->user->name)
                                <a href="{{ route('ad.update', compact('ad')) }}" class="btn btnDetail2">Modifica</a>
                                <div class="marginBtn">
                                    <form method="POST" action="{{ route('ad.delete', compact('ad')) }}">
                                        @csrf
                                        @method("delete")
                                        <button type="submit" class="btn btnDetail3">Elimina</button>
                                    </form>
                                </div>
                            @endif
                        @endauth

                    </div>
                </div>
            </div>

            {{-- CAROSELLO TUOI ANNUNCI NON TOCCARE NON SMINCHIARE --}}
            <div class="col-12 col-md-6 my-5">
                <div class="swiper mySwiper">
                    <!-- Additional required wrapper -->
                    <div class="swiper-button-next"></div>
                    <div class="swiper-wrapper ">
                        <!-- Slides -->
                        @foreach ($ad->images as $image)
                            <div class="col-12 col-md-6 my-3 rounded swiper-slide">
                                <img class='' src="{{ $image->getUrl(300,150) }}" alt="">        
                            </div>
                        @endforeach
                    </div>
                    <div class="swiper-button-prev"></div>
                </div>
            </div>
        </div>
    </div>


    {{-- CAROSELLO TUOI ANNUNCI NON TOCCARE NON SMINCHIARE FINE --}}


    {{-- <div class="col-12 col-md-6">
                <div class="bd-example">
                    <div id="carouselExampleControls" class="carousel slide pointer-event" data-bs-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item">
                               
                           
                                @foreach ($ad->images as $image)
                                <svg class="bd-placeholder-img bd-placeholder-img-lg d-block w-100" width="800"
                                height="400" 
                                xmlns="{{Storage::url($image->file)}}" 
                                role="img"
                                aria-label="Placeholder: First slide" preserveAspectRatio="xMidYMid slice"
                                focusable="false">
                                <title>Placeholder</title>
                                <rect width="100%" height="100%" fill="#777"></rect><text x="50%" y="50%"
                                    fill="#555" dy=".3em">First slide</text>
                                </svg>
                                @endforeach
                            </div>
                            {{-- <div class="carousel-item active">
                                <img src="{{Storage::url($ad->images->file)}}" alt="">
                            </div>
                            <div class="carousel-item">
                                <img src="{{Storage::url($ad->images->file)}}" alt="">
                            </div> --}}
    {{-- </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls"
                            data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"
                            data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>

                </div>
            </div>
        </div>
    </div> --}}




</x-layout>
