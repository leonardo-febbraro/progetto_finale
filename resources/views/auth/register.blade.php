<x-layout>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="registration-form">
        <form method="POST" action="{{ route('register') }}">
            @csrf
            <div class="form-icon">
                <span><i class="fas fa-user-plus"></i></span>
            </div>
            <div class="form-group">
                <input name="name" type="text" class="form-control item" id="username" placeholder="Username">
            </div>
            <div class="form-group">
                <input name="email" type="text" class="form-control item" id="email" placeholder="Email">
            </div>
            <div class="form-group">
                <input name="password" type="password" class="form-control item" id="password" placeholder="Password">
            </div>
            <div class="form-group">
                <input name="password_confirmation" type="password" class="form-control item" id="confirm_password"
                    placeholder="Conferma Password">
            </div>
            <div class="form-group mb-3">
                <button type="submit" class="btn btn-block create-account">Create Account</button>
            </div>
            <a href="{{ route('login') }}" class="text-decoration-none">Hai già un Account?</a>
        </form>
        <div class="social-media">
            <h5>Sign up with social media</h5>
            <div class="social-icons">
                <a href="#"><i class="fab fa-facebook-f" title="Facebook"></i></a>
                <a href="#"><i class="fab fa-instagram" title="Instagram"></i></a>
                <a href="#"><i class="fab fa-twitter" title='Twitter'></i></a>
            </div>
        </div>
    </div>
</x-layout>
