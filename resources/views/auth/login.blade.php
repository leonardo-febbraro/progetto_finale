<x-layout>
    <div class="registration-form">
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-icon">
                <span><i class="fas fa-user-check"></i></span>
            </div>
            <div class="form-group">
                <input name="email" type="text" class="form-control item" id="email" placeholder="Email">
            </div>
            <div class="form-group">
                <input name="password" type="password" class="form-control item" id="password" placeholder="Password">
            </div>
            <div class="form-group mb-3">
                <button type="submit" class="btn btn-block create-account">Login</button>
            </div>
            <a href="{{ route('register') }}" class="text-decoration-none">Non sei ancora registrato?</a>

        </form>
        <div class="social-media">
            <h5>Login with social media</h5>
            <div class="social-icons">
                <a href="#"><i class="fab fa-facebook-f" title="Facebook"></i></a>
                <a href="#"><i class="fab fa-instagram" title="Instagram"></i></a>
                <a href="#"><i class="fab fa-twitter" title='Twitter'></i></a>
            </div>
        </div>
    </div>
</x-layout>
