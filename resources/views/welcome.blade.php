<x-layout>

    @if (session('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
    {{-- Revisore --}}
    @if (session('accesso.rifutato.solo.revisori'))
        <div class="alert alert-danger">
            Accesso non consentito - solo per revisori
        </div>
    @endif
    <header class="container-fluid ">
        <div id="app">
            <div class="title titletle">
                <div class="title-inner" href=>
                    <div class="cafe">
                        <div class="cafe-inner">Presto.it</div>
                    </div>
                    <div class="mozart">
                        <div class="mozart-inner">Annunci</div>
                    </div>


                </div>
            </div>
            <div class="image">
                <img src='img/ecommerce.jpg' alt=''>
            </div>
        </div>
    </header>



    <div class="contenitore">
       <div class="row ">
        <form action="{{ route('search') }}" method="GET">
            @csrf
        <input class="x input" type="text" placeholder="Fai la tua ricerca">
        </form>
        <div class="searche"></div>
       </div>
      </div>

    {{-- <form action="{{ route('search') }}" method="GET">
        @csrf --}}


    {{-- barra di ricerca secondaria --}}
    {{-- <div class="container d-flex justify-content-center">
        <div class="row">
            <div class="col-12 d-flex justify-content-center bg-primary my-2 rounded">
                <h2>RICERCA</h2>
                <div class="mt-3 mb-3">
                    <form action="{{ route('search') }}" method="GET">
                        @csrf
                        <input name="q" type="text" class="" placeholder="Scrivi qui cosa stai cercando">
                        <button class="btn-search rounded">
                        </button>
                    </form>
                </div>
                
            </div>
        </div>
    </div> --}}

    <!-- Card Section -->
    <div class="container-fluid mt-3">
        <div class="row">
            <div class="col-12 d-flex justify-content-center">
                <h1 class="mt-5">{{ __('ui.welcome') }}</h1>
            </div>
        </div>
    </div>
    {{-- <p class="card-text">{{$ad->category->name}}</p> --}}


    <div class="container mt-5">
        <div class="row">

            <div class="swiper mySwiper">
                <!-- Additional required wrapper -->
                <div class="swiper-wrapper ">
                    <!-- Slides -->
                    @foreach ($ads as $ad)
                        <div class="col-12 col-md-6 my-3 rounded swiper-slide">
                            <div class="card">
                                <div class="card-header border-bottom-dark cardColor">
                                    <h4 class="titleCard card-title">{{ $ad->title }}</h4>
                                </div>
                                <div class="card-body cardColor">
                                    <h5 class="priceCard card-text">{{ $ad->price }}€</h5>
                                    <div class="card-img d-flex justify-content-center">
                                        {{-- @foreach ($ad->images as $image)
                                            
                                        <img class='img-fluid w-40' src="{{Storage::url($image->getUrl(300,150))}}" alt="">
                                        @endforeach --}}
                                        <img src="{{($ad->images->first()->getUrl(300,150))}}" alt="">
                                    </div>
                                    <p class="card-text">{{ $ad->created_at->format('d/m/y') }}</p>
                                    <p class="card-text my-3 text-truncate">{{ $ad->description }}</p>
                                    <h6 class="card-text">Annuncio pubblicato da: {{ $ad->user->name }}</h6>
                                    <div class="d-flex justify-content-end mb-1">
                                        <a href="{{ route('ad.show', compact('ad')) }}"
                                            class="btn btnDetail">dettaglio</a>
                                    </div>
                                </div>
                                <div class="card-footer border-top-dark cardColor">
                                    <p class="card-text text-muted">Categoria: {{ $ad->category->name }}</p>
                                </div>
                            </div>
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                        </div>
                    @endforeach
                </div>
            </div>

        </div>
    </div>


    <div class="container-fluid mt-5">
        <div class="row">
            <div class="col-md-6 col-12 ">

                <div class="d-flex no-wrap">

                    <div class="col-3">
                        <i class="fas fa-shipping-fast spaceIcon fa-3x"></i>
                    </div>
                    <div class="col-9">
                        <p class="textIcon"><em>Consegna veloce in tutto il mondo.
                                Possiamo spedire l'ordine in tutto il mondo</em></p>
                    </div>

                </div>
            </div>
            <div class="col-md-6 col-12 ">

                <div class="d-flex no-wrap">

                    <div class="col-3">
                        <i class="fas fa-shipping-fast spaceIcon fa-3x"></i>
                    </div>
                    <div class="col-9">
                        <p class="textIcon"><em>Consegna veloce in tutto il mondo.
                                Possiamo spedire l'ordine in tutto il mondo</em></p>
                    </div>

                </div>
            </div>





        </div>
    </div>


    {{-- CAROSELLO TUOI ANNUNCI NON TOCCARE NON SMINCHIARE --}}
    {{-- <div class="swiper mySwiper"> --}}
    <!-- Additional required wrapper -->
    {{-- <div class="swiper-wrapper "> 
            <!-- Slides -->
            @foreach ($ads as $ad)
                <div class="col-12 col-md-6 my-3 rounded swiper-slide">
                    <div class="card">
                        <div class="card-header border-bottom-dark cardColor">
                            <h4 class="titleCard card-title">{{ $ad->title }}</h4>
                        </div>
                        <div class="card-body cardColor">
                            <h5 class="priceCard card-text">{{ $ad->price }}€</h5>
                            <div class="card-img d-flex justify-content-center">
                                <img class='img-fluid w-40' src="https://picsum.photos/200/150" alt="">
                            </div>
                              <p class="card-text">{{$ad->created_at->format("d/m/y")}}</p> 
                             <p class="card-text my-3 text-truncate">{{ $ad->description }}</p>
                            <h6 class="card-text">Annuncio pubblicato da: {{ $ad->user->name }}</h6>
                            <div class="d-flex justify-content-end mb-1">
                                <a href="{{ route('ad.show', compact('ad')) }}"
                                    class="btn btnDetail">dettaglio</a>
                            </div>
                        </div>
                        <div class="card-footer border-top-dark cardColor">
                            <p class="card-text text-muted">Categoria: {{ $ad->category->name }}</p>
                        </div>
                    </div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>
            @endforeach
        </div>
    </div> --}}
    {{-- CAROSELLO TUOI ANNUNCI NON TOCCARE NON SMINCHIARE FINE --}}


</x-layout>
