<x-layout>


    @if (count($ads) > 0)


        @foreach ($ads as $ad)
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">Annuncio {{ $ad->id }}</div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-2">
                                        <h3>Utente</h3>
                                    </div>
                                    <div class="col-md-10">
                                        {{ $ad->user_id }},
                                        {{ $ad->user->name }},
                                        {{ $ad->user->email }}
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-md-2">
                                        <h3>Titolo</h3>
                                    </div>
                                    <div class="col-md-10">{{ $ad->title }}</div>
                                </div>
                                <hr>

                                <div class="row">
                                    <div class="col-md-2">
                                        <h3>Autore</h3>
                                    </div>
                                    <div class="col-md-10">{{ $ad->user->name }}</div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-2">
                                        <h3>Descrizione</h3>
                                    </div>
                                    <div class="col-md-10">{{ $ad->description }}</div>
                                </div>
                                <hr>


                                <div class="row">
                                    <div class="col-md-2">
                                        <h3>Immagini</h3>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="row mb-2">
                                            <div class="col-md-4">
                                                <img src="https://via.placeholder.com/250x150.png"
                                                    class="rounded" alt="">
                                            </div>
                                            <div class="col-md-8">
                                                ... ... ...
                                            </div>
                                        </div>

                        

            <div class="row justify-content-center mt-5">

                {{-- <div class="col-md-6">
                    <form action="{{route("revisor.undo", $ad->id)}}" method="POST">
                    @csrf
                    <button type="submit" class="btn btn-warning">Undo</button>
                    </form>
                </div> --}}
                <div class="col-md-6">
                    <form action="{{ route('revisor.accept', $ad->id) }}" method="POST">
                        @csrf
                        <button type="submit" class="btn btn-success">Accept</button>
                    </form>
                </div>
                <div class="col-md-6">
                    <form action="{{ route('ad.delete', compact('ad')) }}" method="POST">
                        @method("delete")
                        @csrf
                        <button type="submit" class="btn btn-danger">Reject</button>
                    </form>
                </div>
            </div>
            </div>

        @endforeach

    @else
        <div class="container">
            <div class="row justify-content-center text-center">
                <div class="col-12">
                    <h2 class="mt-4">Non ci sono annunci nel cestino</h2>
                </div>
            </div>
        </div>
    @endif
</x-layout>
