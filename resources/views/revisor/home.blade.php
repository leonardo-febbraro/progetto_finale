<x-layout>


    @if ($ad)

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">Annuncio {{ $ad->id }}</div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-2">
                                    <h3>Utente</h3>
                                </div>
                                <div class="col-md-10">
                                    {{ $ad->user_id }},
                                    {{ $ad->user->name }},
                                    {{ $ad->user->email }}
                                </div>
                            </div>

                            <hr>

                            <div class="row">
                                <div class="col-md-2">
                                    <h3>Titolo</h3>
                                </div>
                                <div class="col-md-10">{{ $ad->title }}</div>
                            </div>



                            <hr>
                            <div class="row">
                                <div class="col-md-2">
                                    <h3>Descrizione</h3>
                                </div>
                                <div class="col-md-10">{{ $ad->description }}</div>
                            </div>
                            <hr>


                            <div class="row">
                                <div class="col-md-2">
                                    <h3>Immagini</h3>
                                </div>
                                <div class="col-md-10">
                                    @foreach ($ad->images as $image)
                                        <div class="row mb-2">
                                            <div class="col-md-4">
                                                <img src="{{ $image->getUrl(300, 150); }}" class="rounded"
                                                    alt="">
                                                    
                                            </div>
                                            <div class="col-md-8">
                                                Adult: {{ $image->adult }} <br>
                                                spoof: {{ $image->spoof }} <br>
                                                medical: {{ $image->medical }} <br>
                                                violence: {{ $image->violence }} <br>
                                                racy: {{ $image->racy }} <br>
                                                {{ $image->id }} <br>
                                                {{ $image->file }} <br>
                                                {{ Storage::url($image->file) }} <br>

                                                <p>Labels</p>
                                                <ul>
                                                    @if ($image->labels)
                                                        @foreach ($image->labels as $label)
                                                            <li>{{ $label }}</li>
                                                        @endforeach
    
                                                    @endif
                                                </ul>
                                            </div>

                                        </div>
                                    @endforeach
                                </div>
                            </div>




                            <div class="row  mt-5">
                                <div class="col-md-6 d-flex justify-content-center ">
                                    <form action="{{ route('revisor.reject', $ad->id) }}" method="POST">
                                        @csrf
                                        <button type="submit" class="btn btn-danger btn-lg">Reject</button>
                                    </form>
                                </div>
                                {{-- <div class="col-md-6">
                                            <form action="{{route("revisor.undo", $ad->id)}}" method="POST">
                                            @csrf
                                            <button type="submit" class="btn btn-warning">Undo</button>
                                            </form>
                                        </div> --}}
                                <div class="col-md-6 d-flex justify-content-center ">
                                    <form action="{{ route('revisor.accept', $ad->id) }}" method="POST">
                                        @csrf
                                        <button type="submit" class="btn btn-success btn-lg">Accept</button>
                                    </form>
                                </div>
                            </div>


                            @else
                            <div class="container">
                                <div class="row justify-content-center text-center">
                                    <div class="col-12">
                                        <h2 class="mt-4">Non ci sono annunci da revisionare</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @endif
</x-layout>
