<nav class="navbar navbar-expand-lg navbarCustom">
    <div class="container">
      <a class="navbar-brand p-0 m-0" href="{{ route('welcome') }}">
    <img class="img-fluid iconNavbar" src="/img/logo2.png" alt="">
    </a>
      <button id="navbarToggler" class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <i id="arrow" class="far fa-arrow-alt-circle-down fa-2x arrowCustom"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link active textAncor" aria-current="page" href="{{route('welcome')}}">Home</a>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle textAncor" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              Shop
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">


              <li class="nav-item dropdown"><a class="dropdown-item my-1" href="{{route('ad.allAds')}}">Tutte le categorie</a></li>
              @foreach ($categories as $category)
                <li class="nav-item dropdown"><a class="dropdown-item my-1" href="{{ route('ad.categories', [
                  $category->name,
                  $category->id
                ])}}"
                >{{$category->name}}</a></li>
              @endforeach
              
              

              
            </ul>
          </li>

          <li class="nav-item">
            <a class="nav-link active textAncor" href="{{ route('ad.form') }}">Inserisci annuncio</a>
          </li>
        </ul>
        <li class="nav-item flag-dots">
          <form action="{{route("locale", "it")}}" method="POST">
          @csrf
            <button type="submit" class="nav-link" style="background-color: transparent; border:none">
              <span class="flag-icon flag-icon-it"></span>
            </button>
          </form>
        </li>
        <li class="nav-item flag-dots">
          <form action="{{route("locale", "en")}}" method="POST">
          @csrf
            <button type="submit" class="nav-link" style="background-color: transparent; border:none">
              <span class="flag-icon flag-icon-gb"></span>
            </button>
          </form>
        </li>
        <li class="nav-item flag-dots">
          <form action="{{route("locale", "es")}}" method="POST">
          @csrf
            <button type="submit" class="nav-link" style="background-color: transparent; border:none">
              <span class="flag-icon flag-icon-es"></span>
            </button>
          </form>
        </li>
        <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
          @guest
          <li class="nav-item">
            <a class="nav-link textAncor" href="{{route('register')}}">Registrati</a>
          </li>
          <li class="nav-item">
            <a class="nav-link textAncor" href="{{route('login')}}">Login</a>
          </li>
          @else
          @if (Auth::user()->is_revisor)
            <li class="nav-item">
              <a class="nav-link textAncor" href="{{route("revisor.home")}}">
                Revisor home
                <span class="badge badge-pill badge-primary">
                  {{\App\Models\Ad::ToBeRevisionedCount()}}
                </span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link textAncor" href="{{route("revisor.creel")}}">
                Cestino
                <span class="badge badge-pill badge-primary">
                  {{\App\Models\Ad::ToBeCreelCount()}}
                </span>
              </a>
            </li>
          @endif
          <li class="d-flex align-items-center">
            <p class="benvCustom">
              Benvenuto {{Auth::user()->name}}!
            </p>
          </div>
            <div id="bubblePosition" class="position-relative">
              <img id="dropDown1" class="img-fluid iconDropdown" src="/img/iconaDent.png" alt="icona">
              <img id="dropDown2" class="img-fluid iconDropdown d-none" src="/img/icona_X.png" alt="icona">
              <dl id="dropDownlist" class="bubble">
            
                <dd class="list1">
                  <a class="bubbleList" href="">?</a>
                </dd>
                <dd class="list2">
                  <a class="bubbleList" href="">?</a>
                </dd>
                <dd class="list3 bubbleList" href="{{route('logout')}}"
                  onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();">
              Logout
              
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
              @csrf
          </form>
        </dd>
              </dl>
              </div>

            
          </li>
          @endguest
  
        </ul>

    </div>
  </nav>


  


