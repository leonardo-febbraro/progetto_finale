<!--  Footer Section  -->

<footer class="container-fluid footerBody my-3">
    <div class="row moveSpace marginFooter justify-content-evenly">
        <div class="col-6 col-lg-3 my-3">
            <div class="text-uppercase textColor2">
                Social
            </div>
            <div class="textColor mb-3 my-3">
                Link associati per qualsiasi info puoi contattarci tramite i tre link forniti
            </div>
            <div class="iconSocial mt-4">
                <i class="fab fa-facebook iconSocial2 fa-3x">
                    <a href="#"></a>
                </i>
                <i class="fab fa-whatsapp iconSocial2 fa-3x">
                    <a href="#"></a>
                </i>
                <i class="fab fa-instagram iconSocial2 fa-3x">
                    <a href="#"></a>
                </i>
            </div>
        </div>

        {{-- Info compagnia + info supporto --}}

        {{-- <div class="col-6 col-lg-3 my-3 ">
            <div class="text-uppercase textColor2 mb-4">
                info compagnia</div>
            <ul class="list-unstyled mb-0">
                <li class="mb-2">
                    <a class="textMain" href="/themes">Chi siamo</a>
                </li>
                <li class="mb-2">
                    <a class="textMain" href="/snippets">News</a>
                </li>
                <li>
                    <a class="textMain" href="/guides">Guide</a>
                </li>
            </ul>
        </div> --}}


        {{-- <div class="col-6 col-lg-3 my-3">
            <div class="text-uppercase textColor2 mb-4">Aiuto e Supporto</div>
            <ul class="list-unstyled mb-0">
                <li class="mb-2"><a class="textMain" href="/blog">Pagamenti</a></li>
                <li class="mb-2"><a class="textMain" href="/bootstrap-resources">Termini di
                        Servizio</a>
                </li>
                <li><a class="textMain" href="/sb-angular-inspector">Privacy Policy</a></li>
            </ul>
        </div> --}}
        <div class="col-6 col-lg-3 my-3">
            <div class="text-uppercase textColor2 mb-4">Contatti</div>
            <ul class="list-unstyled mb-0">
                <li class="mb-2">
                    <p class="textColor"><strong>Email :</strong>
                        <a class="textMain" href="/templates">presto90@gmail.com</a>
                    </p>
                </li>
                <li class="mb-2">
                    <p class="textColor"><strong>Cell :</strong>
                        <a class="textMain" href="/templates">+39 3969993959</a>
                    </p>
                </li>
                <li class="mb-2">
                    <p class="textColor"><strong>Via :</strong>
                        <a class="textMain" href="/templates">della Gloria n° 23</a>
                    </p>
                </li>
            </ul>
        </div>
        
    </div>
    </div>
    </div>
</footer>

<!--  Finish  -->

<div class="container-fluid d-flex justify-content-center">
    <div class="row">
        <div class=" barraSp"></div>
        <div class="text-lg-center copyright">Copyright © Shopping Online 2021</div>
    </div>
</div>
