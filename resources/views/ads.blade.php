<x-layout>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <h1>Cerca l'annuncio per categoria</h1>
            </div>
        </div>
        @if (session('message'))
                <div class="alert alert-success">
                    {{ session('message') }}
                </div>
            @endif
            @if (session('messageDelete'))
                <div class="alert alert-danger">
                    {{ session('messageDelete') }}
                </div>
            @endif
        <div class="row">
            @foreach ($ads as $ad)          
            <div class="col-12 col-md-6">
                <div class="card" style="width: 18rem;">
                    
                    <div class="card-body">
                      <h5 class="card-title">{{$ad->title}}</h5>
                      <p class="priceCard card-text">{{$ad->description}}</p>
                      <p class="card-text">{{$ad->price}}€</p>
                      <p class="card-text">{{$ad->category->name}}</p>
                      <p class="card-text">{{$ad->user->name}}</p>
                      <img src="https://via.placeholder.com/150" alt="">
                      <p class="card-text">{{$ad->created_at->format("d/m/y")}}</p>
    
                      
                    </div>
                  </div>
                
            </div>
            @endforeach
        </div>
        {{$ads->links()}}
    </div>
</x-layout>