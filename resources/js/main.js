// bubble window
// Script del menù icona della navbar
let dropDown1 = document.querySelector('#dropDown1');
let dropDownlist = document.querySelector('#dropDownlist');
let dropDown2 = document.querySelector("#dropDown2");
if(dropDown1){

	dropDown1.addEventListener("click", () => {
		dropDownlist.classList.add("bubble2");
		dropDown1.classList.add("d-none");
		dropDown2.classList.remove("d-none");
	})
}


if(dropDown2){

	dropDown2.addEventListener("click", () => {
		dropDownlist.classList.remove("bubble2");
		dropDown1.classList.remove("d-none");
		dropDown2.classList.add("d-none");
	
	})
}



let arrow = document.querySelector('#arrow');
let navbarToggler = document.querySelector('#navbarToggler');
navbarToggler.addEventListener("click", () => {
	arrow.classList.toggle('fa-arrow-alt-circle-down');
	arrow.classList.toggle('fa-arrow-alt-circle-up');
})


let bubblePosition = document.querySelector('#bubblePosition');
navbarToggler.addEventListener("click", () => {
	console.log('click');
	bubblePosition.classList.toggle('bubblePosition');
})

var swiper = new Swiper(".mySwiper", {
	effect: "coverflow",
	grabCursor: true,
	centeredSlides: true,
	slidesPerView: "auto",
	coverflowEffect: {
		rotate: 50,
		stretch: 0,
		depth: 100,
		modifier: 1,
		slideShadows: false,
	},
	autoplay: {
		delay: 5000,
		disableOnInteraction: false,
	},
	navigation: {
		nextEl: ".swiper-button-next",
		prevEl: ".swiper-button-prev",
	},
});