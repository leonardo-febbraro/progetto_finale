<?php

namespace App\Models;


use App\Models\User;
use App\Models\AdImage;
use App\Models\Category;
use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Ad extends Model
{
    use Searchable;
    use HasFactory;

    protected $fillable=[
        'title',
        'description',
        'price',
        "user_id",
        "category_id",
        "img",
    ];

    public function toSearchableArray()
    {
        $array=[
            "id"=>$this->id,
            "title"=>$this->title,
            "description"=>$this->description,
            "price"=>$this->price,

        ];

        

        return $array;
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

    static public function ToBeRevisionedCount()
    {
        return Ad::where("is_accepted", null)->count();
    }

    static public function ToBeCreelCount()
    {
        return Ad::where("is_accepted", 0)->count();
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function images()
    {
        return $this->hasMany(AdImage::class);
    }
}
