<?php

namespace App\Http\Controllers;

use App\Models\Ad;
use App\Models\AdImage;
use App\Models\Category;
use App\Jobs\ResizeImage;
use Illuminate\Http\Request;
use App\Jobs\GoogleVisionImage;
use App\Http\Requests\UpdateRequest;
use App\Jobs\GoogleVisionLabelImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Jobs\GoogleVisionRemoveFaces;
use Illuminate\Support\Facades\Storage;
use App\Jobs\GoogleVisionSafeSearchImage;

class AdController extends Controller
{   
    public function __construct()
    {
        $this->middleware("auth")->except('categories', 'show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index()
    // {
    //     $ads= Ad::orderBy('created_at', 'DESC')->paginate(6);
    //     return view('ad.allAds', compact('ads'));
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $uniqueSecret= $request->old("uniqueSecret", base_convert(sha1(uniqid(mt_rand())), 16, 36));
        $categories = Category::all();
        return view("ad.form", compact('categories', "uniqueSecret"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {
    
        $a= Ad::create([
            'title'=>$request->title,
            'description'=>$request->description,
            "user_id"=>Auth::id(),
            'category_id'=>$request->category,
            'price'=> $request->price,
        ]);
        // $ad = Auth::user()->ad()->create([
        //     'title' => $request->input('title'),
        //     'description' => $request->input('description'),
        //     'category' => $request->input('category_id'),
        // ]);
            
            
            $uniqueSecret=$request->input("uniqueSecret");
            
            $images=session()->get("images.{$uniqueSecret}",[]);
            $removedImages=session()->get("removedimages.{$uniqueSecret}", []);

            $images= array_diff($images, $removedImages);

            foreach ($images as $image){
                $i= new AdImage();

                $fileName=basename($image);
                $newFileName="public/ads/{$a->id}/{$fileName}";
                Storage::move($image,$newFileName);
                
                
                $i->file=$newFileName;
                $i->ad_id=$a->id;
                
                $i->save();
                
               
             
                GoogleVisionSafeSearchImage::withChain([
                    new GoogleVisionLabelImage($i->id),
                    new GoogleVisionRemoveFaces($i->id),
                    new ResizeImage($newFileName, 300, 150)
                ])->dispatch($i->id);

            }
            
        File::deleteDirectory(storage_path("/app/public/temp/{$uniqueSecret}"));
            
        return redirect(route('ad.allAds'))->with('message', 'il tuo annuncio è stato inserito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function show(Ad $ad)
    { 
            return view("ad.show", compact("ad"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function edit( Request $request, Ad $ad)
    {
        
        $ad->title=$request->title;
        $ad->price=$request->price;
        $ad->description=$request->description;
        $ad->save();

        return redirect(route("welcome"))->with("message", "Il tuo annuncio è stato modificato");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function update(Ad $ad){

        return view(("ad.update"), compact("ad"));
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ad $ad)
    {
        $ad->delete();
        return redirect(route("welcome"))->with("message", "Il tuo annuncio è stato cancellato");
    }

    public function categories($name, $id){
        $category= Category::find($id);
        $ads= $category->ads()
        ->where("is_accepted", true)
        ->orderBy('created_at', 'DESC')
        ->paginate(6);
        // ->take(6)
        // ->get();
        return view('ad.allForCategory', compact('category', 'ads'));
    }

    public function user(){
        $ads= Auth::user()->ads->get();
        
        return view("ad.allAds", compact("ads"));
    }
    
    public function uploadImage(Request $request)
    {
        $uniqueSecret=$request->input("uniqueSecret");
        $fileName=$request->file("file")->store("public/temp/{$uniqueSecret}");

        dispatch(new ResizeImage(
            $fileName,
            120,
            120
        ));
        
        session()->push("images.{$uniqueSecret}", $fileName);
        return response()->json(
            [
                "id"=>$fileName
            ]
        );
    }
    
    public function removeImage(Request $request)
    {
        $uniqueSecret=$request->input("uniqueSecret");
        $fileName=$request->input("id");
        session()->push("removedimages.{$uniqueSecret}", $fileName);
        Storage::delete($fileName);
        return response()->json("ok");
    }

    public function getImages(Request $request)
    {
        $uniqueSecret=$request->input("uniqueSecret");
        $images=session()->get("images.{$uniqueSecret}", []);
        $removedImages=session()->get("removedimages.{$uniqueSecret}", []);

        $images=array_diff($images,$removedImages);

        $data=[];

        foreach ($images as $image){
            $data[]=[
                "id"=>$image,
                // "src"=>Storage::url($image)
                'src'=> AdImage::getUrlByFilePath($image, 120, 120)
            ];
        }

        return response()->json($data);
    }

}
