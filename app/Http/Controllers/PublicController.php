<?php

namespace App\Http\Controllers;

use App\Models\Ad;
use App\Models\Category;
use Illuminate\Http\Request;

class PublicController extends Controller
{
    public function welcome() {
        $ads= Ad::where("is_accepted", true)
        ->orderBy('created_at', 'DESC')
        ->take(5)
        ->get();
        return view('welcome', compact('ads'));
    }

    public function index()
    {
        $ads= Ad::where("is_accepted", true)
        ->orderBy('created_at', 'DESC')
        ->paginate(6);
        // ->take(6)
        // ->get();
        return view('ad.allAds', compact('ads'));
    }
    
    // public function ads($name, $category_id){
    //     $category = Category::find($category_id);
    //     $ads = $category->ads();
    //     return view("ad.allAds", compact("category", "ads"));
    // }

    // public function adsByCategory($name, $category_id)
    // {
    //     $category=Category::find($category_id);
    //     $ads=$category->ads()->orderBy("created_at", "DESC")->paginate(6);
    //     return view("ads", compact("category", "ads"));
    // }

    public function search(Request $request){
        $q=$request->input("q");
        
        $ads=Ad::search($q)->where("is_accepted",true)->get();
        
        return view("search_result", compact("q", "ads"));
    }

    public function locale($locale)
    {
        session()->put("locale", $locale);
        return redirect()->back();
    }
}
