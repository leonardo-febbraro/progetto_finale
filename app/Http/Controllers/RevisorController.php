<?php

namespace App\Http\Controllers;

use App\Models\Ad;
use App\Models\AdImage;
use Illuminate\Http\Request;

class RevisorController extends Controller
{
    public function __construct()
    {
        $this->middleware("auth.revisor");
    }

    public function index()
    {
        
        $ad = Ad::where("is_accepted", null)
            ->orderBy("created_at", "DESC")
            ->first();
        
        
        return view("revisor.home", compact("ad"));
    }

    public function creel()
    {
        $ads = Ad::where("is_accepted", false)
            ->orderBy('created_at', 'DESC')
            ->get();
        // ->take(6)
        // ->get()
        return view('revisor.creel', compact('ads'));
    }


    private function setAccepted($ad_id, $value)
    {
        $ad = Ad::find($ad_id);
        $ad->is_accepted = $value;
        $ad->save();
        return redirect(route("revisor.home"));
    }

    public function accept($ad_id)
    {
        return $this->setAccepted($ad_id, true);
    }

    public function reject($ad_id)
    {
        return $this->setAccepted($ad_id, false);
    }

    // public function undo($ad_id)
    // {
    //     if($ad_id>1){
    //         $ad= Ad::where('id', $ad_id-1)->update(['is_accepted' => NULL]);
    //         return $this->setAccepted($ad_id,false);
    //     }

    //     $ad=Ad::where("is_accepted", null)
    //         ->orderBy("created_at", "DESC")
    //         ->first();
    //     return view("revisor.home", compact("ad"));

    // }


}
