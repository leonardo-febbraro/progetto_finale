<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdController;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\RevisorController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'welcome'] )->name('welcome');
Route::get("/ad/form", [AdController::class, "create"])->name("ad.form");
Route::post("/ad/store", [AdController::class, "store"])->name("new.ad");
Route::get("/ad/allAds", [PublicController::class, "index"])->name("ad.allAds");
Route::get("/ad/detail/{ad}", [AdController::class, "show"])->name("ad.show");
Route::get("/ad/category/{name}/{id}", [AdController::class, "categories"])->name("ad.categories");
Route::get("/category/{name}/{id}/ads", [PublicController::class, "ads"])->name("ads");
Route::get("/ad/search", [PublicController::class, "search"])->name("search");
Route::get("/ad/update/{ad}", [AdController::class, "update"])->name("ad.update");
Route::put("/ad/edit/{ad}", [AdController::class, "edit"])->name("ad.edit");
Route::delete("/ad/destroy/{ad}", [AdController::class, "destroy"])->name("ad.delete");
Route::post("/ad/images/upload",[AdController::class, "uploadImage"])->name("ad.imageUpload");
Route::delete("/ad/images/remove", [AdController::class, "removeImage"])->name("ad.imageRemove");
Route::get("/ad/images", [AdController::class, "getImages"])->name("ad.images");
Route::post("/locale/{locale}", [PublicController::class, "locale"])->name("locale");

//Rotta revisore
Route::get("/revisor/home", [RevisorController::class, "index"])->name("revisor.home");
Route::post("/revisor/ad/{id}/accept", [RevisorController::class, "accept"])->name("revisor.accept");
Route::post("/revisor/ad/{id}/reject", [RevisorController::class, "reject"])->name("revisor.reject");
// Route::post("/revisor/ad/{id}/undo", [RevisorController::class, "undo"])->name("revisor.undo");
Route::get("/creel/home", [RevisorController::class, "creel"])->name("revisor.creel");







